﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio9
{
    class Program
    {
        static void Main(string[] args)
        {
            int Num, R;
            Console.Write("Ingrese el numero de la tabla de multiplicar que see del 1-15: ");
            Num = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < 15; i++)
            {
                R = i * Num;
                Console.WriteLine("{0} x {1} = {2}", i, Num, R);

            }

            Console.ReadKey();
        }
    }
}
