﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int x=0, y=101;
            
            do
            {
                x++;
                y--;
                Console.WriteLine(" {0} {1}", x, y);
            } while (x < 101 && y > 1);
            
            Console.ReadKey();
        }
    }
}
